#include <ros/ros.h>
#include <mutex>
#include <vector>

#include <sensor_msgs/PointCloud2.h>
#include <darknet_ros_msgs/BoundingBoxes.h>

bool isBoundingBoxesAvailable = false;
std::vector<darknet_ros_msgs::BoundingBox> lastBoundingBoxes;
std::mutex lastBoundingBoxesMutex;

void boundingBoxesCallbackSubscriber(const darknet_ros_msgs::BoundingBoxesConstPtr& message) {
    {
        std::lock_guard<std::mutex> lockGuard(lastBoundingBoxesMutex);
        lastBoundingBoxes.clear();
    }
    size_t foundClasses = message->bounding_boxes.size();
    for(size_t index = 0; index < foundClasses; index++) {
        if (message->bounding_boxes.at(index).Class.compare("person") == 0) {
            {
                std::lock_guard<std::mutex> lockGuard(lastBoundingBoxesMutex);
                lastBoundingBoxes.push_back(message->bounding_boxes.at(index));
            }
        }
    }
    isBoundingBoxesAvailable = true;
}

bool isPointCloudAvailable = false;
sensor_msgs::PointCloud2 lastPointCloud;
std::mutex lastPointCloudMutex;

void pointCloudCallbackSubscriber(const sensor_msgs::PointCloud2ConstPtr& message) {
    std::lock_guard<std::mutex> lockGuard(lastPointCloudMutex);
    lastPointCloud = *message;
    isPointCloudAvailable = true;
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "PersonDepthFounder");

    ros::NodeHandle node;

    ros::Subscriber boundingBoxesSubscriber = node.subscribe<darknet_ros_msgs::BoundingBoxes>("/darknet_ros/bounding_boxes",1000,boundingBoxesCallbackSubscriber);
    ros::Subscriber pointCloudSubscriber = node.subscribe<sensor_msgs::PointCloud2>("/elp/points2",1000,pointCloudCallbackSubscriber);

    ros::Publisher  publisher = node.advertise<sensor_msgs::PointCloud2>("/elp/person_points2",1000);

    ros::Rate loop(5);

    while(ros::ok()) {
        if(isBoundingBoxesAvailable && isPointCloudAvailable) {
            std::vector<darknet_ros_msgs::BoundingBox> currentBoundingBoxes;
            {
                std::lock_guard<std::mutex> lockGuard(lastBoundingBoxesMutex);
                currentBoundingBoxes.reserve(lastBoundingBoxes.size());
                currentBoundingBoxes = lastBoundingBoxes;
            }

            sensor_msgs::PointCloud2 currentPointCloud;
            {
                std::lock_guard<std::mutex> lockGuard(lastPointCloudMutex);
                currentPointCloud = lastPointCloud;
            }

            sensor_msgs::PointCloud2 personPointCloud;
            personPointCloud = currentPointCloud;

            std::vector<float> validBoundingBoxesDepth;

            for(int boundingBoxIndex = 0; boundingBoxIndex < currentBoundingBoxes.size(); boundingBoxIndex++) {
                darknet_ros_msgs::BoundingBox boundingBox = currentBoundingBoxes.at(boundingBoxIndex);
                int boundingBoxHeightCenter = (boundingBox.ymin + boundingBox.ymax) / 2;
                int boundingBoxWidthCenter = (boundingBox.xmin + boundingBox.xmax) / 2;

                bool isFound = false;

                while(!isFound) {
                    for(int radiusIndex = 0; radiusIndex < 50 && !isFound; radiusIndex++) {
                        for(int pointIndex = 0; pointIndex < 9 && !isFound; pointIndex++) {
                            int currentHeightCenter = boundingBoxHeightCenter + radiusIndex * ((pointIndex / 3) - 1);
                            int currentWidthCenter = boundingBoxWidthCenter + radiusIndex * ((pointIndex % 3) - 1);

                            int depthImagePosition = currentHeightCenter * personPointCloud.row_step + currentWidthCenter * personPointCloud.point_step;

                            int depthPosition = depthImagePosition + personPointCloud.fields[2].offset;

                            float depth = 0.0;

                            memcpy(&depth, &personPointCloud.data[depthPosition], sizeof(float));

                            if(depth > 0.0) {
                                isFound = true;
                                std::cout << "BoundingBox : " << depth << std::endl;
                                validBoundingBoxesDepth.push_back(depth);
                            }
                        }
                    }
                    if(!isFound) {
                        isFound = true;
                        validBoundingBoxesDepth.push_back(0);
                    }
                }
            }

            for(int heightIndex = 0; heightIndex < personPointCloud.height; heightIndex++) {
                for(int widthIndex = 0; widthIndex < personPointCloud.width; widthIndex++) {
//                    int depthImagePosition = heightIndex * personPointCloud.row_step + widthIndex * personPointCloud.point_step;

//                    int arrayPositionX = depthImagePosition + personPointCloud.fields[0].offset;
//                    int arrayPositionY = depthImagePosition + personPointCloud.fields[1].offset;
//                    int arrayPositionZ = depthImagePosition + personPointCloud.fields[2].offset;
//                    int arrayPositionRGB = depthImagePosition + personPointCloud.fields[3].offset;

//                    float X = 0.0;
//                    float Y = 0.0;
//                    float Z = 0.0;

//                    memcpy(&X, &personPointCloud.data[arrayPositionX], sizeof(float));
//                    memcpy(&Y, &personPointCloud.data[arrayPositionY], sizeof(float));
//                    memcpy(&Z, &personPointCloud.data[arrayPositionZ], sizeof(float));

                    bool isPointValid = false;
                    for(int boundingBoxIndex = 0; boundingBoxIndex < currentBoundingBoxes.size(); boundingBoxIndex++) {
                        darknet_ros_msgs::BoundingBox boundingBox = currentBoundingBoxes.at(boundingBoxIndex);

                        if(heightIndex > boundingBox.ymin && heightIndex < boundingBox.ymax) {
                            if(widthIndex > boundingBox.xmin && widthIndex < boundingBox.xmax) {
                                int depthImagePosition = heightIndex * personPointCloud.row_step + widthIndex * personPointCloud.point_step;
                                int depthPosition = depthImagePosition + personPointCloud.fields[2].offset;
                                float depth = 0;
                                memcpy(&depth, &personPointCloud.data[depthPosition], sizeof(float));

                                if(std::abs(validBoundingBoxesDepth.at(boundingBoxIndex) - depth) < 0.5) {
                                    isPointValid = true;
                                }
                            }
                        }
                    }

                    if(!isPointValid) {
                        int depthImagePosition = heightIndex * personPointCloud.row_step + widthIndex * personPointCloud.point_step;

                        for(int offsetIndex = personPointCloud.fields[3].offset; offsetIndex < personPointCloud.point_step; offsetIndex++) {
                            personPointCloud.data[depthImagePosition + offsetIndex] = 0;
                        }
                    }
                }
            }

            publisher.publish(personPointCloud);
        }

        ros::spinOnce();
        loop.sleep();
    }

    ros::shutdown();

    return 0;
}
