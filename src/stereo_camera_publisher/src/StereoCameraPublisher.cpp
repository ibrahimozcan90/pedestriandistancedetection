#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sstream>

int videoDevice001 = 0;
int videoDevice002 = 0;

int main(int argc, char **argv) {
    ros::init(argc, argv, "StereoCameraPublisher");

    ros::NodeHandle node;

    node.param<int>("videoDevice001", videoDevice001, 2);
    node.param<int>("videoDevice002", videoDevice002, 1);

    image_transport::ImageTransport imageTransport(node);
    image_transport::Publisher darknetPublisher = imageTransport.advertise("/camera/rgb/image_raw", 1);
    image_transport::Publisher publisher001 = imageTransport.advertise("/elp/left/image_raw", 1);
    image_transport::Publisher publisher002 = imageTransport.advertise("/elp/right/image_raw", 1);

    cv::VideoCapture videoCapture001;
    videoCapture001.open(videoDevice001);
    cv::VideoCapture videoCapture002;
    videoCapture002.open(videoDevice002);

    if(!videoCapture001.isOpened() || !videoCapture002.isOpened()) {
    //if(!videoCapture001.isOpened()) {
        std::cout << videoDevice001 << " NOT OPENNED" << std::endl;
        ros::shutdown();
        return 1;
    }

    cv::Mat frame001;
    cv::Mat frame002;
    sensor_msgs::ImagePtr imageMessage001;
    sensor_msgs::ImagePtr imageMessage002;

    ros::Rate loop(20);

    while(ros::ok()) {
        videoCapture001 >> frame001;
        videoCapture002 >> frame002;

        if(!frame001.empty()) {
            imageMessage001 = cv_bridge::CvImage(std_msgs::Header(), "bgr8", frame001).toImageMsg();
            publisher001.publish(imageMessage001);
            darknetPublisher.publish(imageMessage001);
            cv::waitKey(1);
        }

        if(!frame002.empty()) {
            imageMessage002 = cv_bridge::CvImage(std_msgs::Header(), "bgr8", frame002).toImageMsg();
            publisher002.publish(imageMessage002);
            cv::waitKey(1);
        }

        ros::spinOnce();
        loop.sleep();
    }

    videoCapture001.release();
    ros::shutdown();

	return 0;
}
